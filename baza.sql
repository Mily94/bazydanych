-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 20 Maj 2017, 20:40
-- Wersja serwera: 10.1.21-MariaDB
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `szereg`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `administrator`
--

CREATE TABLE `administrator` (
  `id` int(11) NOT NULL,
  `login` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `haslo` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `administrator`
--

INSERT INTO `administrator` (`id`, `login`, `haslo`) VALUES
(1, 'admin1', 'haslo1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id` int(11) NOT NULL,
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `telefon` text COLLATE utf8_polish_ci NOT NULL,
  `kartarabatowa` text COLLATE utf8_polish_ci NOT NULL,
  `login` text COLLATE utf8_polish_ci NOT NULL,
  `haslo` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`id`, `imie`, `nazwisko`, `telefon`, `kartarabatowa`, `login`, `haslo`) VALUES
(1, 'Stefan', 'Banach', '123456789', 'tak', 'klient1', 'haslo1'),
(2, 'Marcin', 'Nowak', '1234', 'tak', 'klient2', 'haslo2'),
(3, 'Tomek', 'Jankowski', '456789', 'nie', 'klient3', 'haslo3'),
(4, 'Marian', 'Jankowski', '13456789', 'nie', 'klient4', 'haslo4'),
(5, 'Aleksandra', 'Szczecina', '123489', 'tak', 'klient5', 'haslo5'),
(6, 'Monika', 'Budzich', '1234789', 'nie', 'klient6', 'haslo6');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kolejnosc`
--

CREATE TABLE `kolejnosc` (
  `id` int(11) NOT NULL,
  `id_zlecenie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rabat`
--

CREATE TABLE `rabat` (
  `id` int(11) NOT NULL,
  `procent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `rabat`
--

INSERT INTO `rabat` (`id`, `procent`) VALUES
(1, 5),
(2, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stawka`
--

CREATE TABLE `stawka` (
  `id` int(11) NOT NULL,
  `wartosc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `stawka`
--

INSERT INTO `stawka` (`id`, `wartosc`) VALUES
(1, 100);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `usluga`
--

CREATE TABLE `usluga` (
  `id` int(11) NOT NULL,
  `nazwa` text COLLATE utf8_polish_ci NOT NULL,
  `czastrwania` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `usluga`
--

INSERT INTO `usluga` (`id`, `nazwa`, `czastrwania`) VALUES
(1, 'wiercenie_1', 1),
(2, 'wiercenie_2', 2),
(3, 'wiercenie_3', 3),
(4, 'wiercenie_4', 4),
(5, 'wiercenie_5', 5),
(6, 'wiercenie_6', 6),
(7, 'wiercenie_7', 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zlecenia`
--

CREATE TABLE `zlecenia` (
  `id` int(11) NOT NULL,
  `id_klient` int(11) NOT NULL,
  `id_stawka` int(11) NOT NULL,
  `id_rabat` int(11) NOT NULL,
  `id_usluga` int(11) NOT NULL,
  `dostepnosc` int(11) NOT NULL,
  `planowanytermin` int(11) NOT NULL,
  `termin` int(11) NOT NULL,
  `planowanykoszt` int(11) NOT NULL,
  `kwotarabatu` int(11) NOT NULL,
  `kosztuslugi` int(11) NOT NULL,
  `status` text COLLATE utf8_polish_ci NOT NULL,
  `czas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zlecenia`
--

INSERT INTO `zlecenia` (`id`, `id_klient`, `id_stawka`, `id_rabat`, `id_usluga`, `dostepnosc`, `planowanytermin`, `termin`, `planowanykoszt`, `kwotarabatu`, `kosztuslugi`, `status`, `czas`) VALUES
(1, 1, 1, 0, 5, 1, 9, 0, 0, 0, 0, 'oczekiwanie', 0),
(2, 3, 1, 0, 5, 4, 4, 0, 0, 0, 0, 'oczekiwanie', 0),
(3, 2, 1, 0, 4, 1, 6, 0, 0, 0, 0, 'oczekiwanie', 0),
(4, 5, 1, 0, 3, 7, 3, 0, 0, 0, 0, 'oczekiwanie', 0),
(5, 4, 1, 0, 6, 3, 8, 0, 0, 0, 0, 'oczekiwanie', 0),
(6, 6, 1, 0, 7, 4, 1, 0, 0, 0, 0, 'oczekiwanie', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `klient`
--
ALTER TABLE `klient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kolejnosc`
--
ALTER TABLE `kolejnosc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rabat`
--
ALTER TABLE `rabat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stawka`
--
ALTER TABLE `stawka`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usluga`
--
ALTER TABLE `usluga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zlecenia`
--
ALTER TABLE `zlecenia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `administrator`
--
ALTER TABLE `administrator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `klient`
--
ALTER TABLE `klient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `kolejnosc`
--
ALTER TABLE `kolejnosc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `rabat`
--
ALTER TABLE `rabat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `stawka`
--
ALTER TABLE `stawka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `usluga`
--
ALTER TABLE `usluga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `zlecenia`
--
ALTER TABLE `zlecenia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
