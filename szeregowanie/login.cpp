#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include "login.h"
#include "ui_login.h"
#include "paneladmina.h"
#include "panelklienta.h"
#include "createacc.h"

Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN1");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("szereg");
    if(db.open()){
        ui->loginFailed->setText("Połączono z bazą danych");
    }
    else{
        ui->loginFailed->setText("Błąd połączenia z bazą");
    }

}

Login::~Login()
{
    delete ui;
}

void Login::on_loginBtn_clicked()
{
    QString Pytanie;
    QString login = ui->username_line->text();
    QString haslo_napisane = ui->password_line->text();
    QString ID;
    QString haslo_prawdziwe;
    QSqlDatabase db = QSqlDatabase::database("CONN1");
    QSqlQuery zapytanie(db);
    Pytanie = "SELECT * FROM `klient` WHERE `login` = '";
    Pytanie += login;
    Pytanie += "'";
    zapytanie.exec(Pytanie);
    if(zapytanie.size()>0){
        while(zapytanie.next()){
        haslo_prawdziwe = zapytanie.value("haslo").toString().toUtf8().constData();
        ID = zapytanie.value("id").toString();
        }
        if(haslo_napisane == haslo_prawdziwe){
            Klient = new PanelKlienta;
            connect(this,SIGNAL(logged(QString)),Klient,SLOT(got_client_nr(QString)));
            connect(Klient,SIGNAL(finished()),this,SLOT(show()));
            Klient->show();
            emit logged(ID);
            this->hide();
        }
        else{
            ui->loginFailed->setText("Błędny login lub hasło");
        }
    }
    else if((login == "admin") && (haslo_napisane == "admin")){
            Admin = new PanelAdmina;
            connect(Admin,SIGNAL(finished()),this,SLOT(show()));
            Admin->show();
            this->hide();
    }
    else{
        ui->loginFailed->setText("Błędny login lub hasło");
    }
}

void Login::on_createAccBtn_clicked()
{
    NKonto = new CreateAcc;
    connect(NKonto,SIGNAL(finished()),this,SLOT(show()));
    connect(NKonto,SIGNAL(registered(QString)),this,SLOT(new_account(QString)));
    NKonto->show();
    this->hide();

}

void Login::new_account(QString ID)
{
    Klient = new PanelKlienta;
    connect(this,SIGNAL(logged(QString)),Klient,SLOT(got_client_nr(QString)));
    connect(Klient,SIGNAL(finished()),this,SLOT(show()));
    Klient->show();
    emit logged(ID);
    this->hide();
}
