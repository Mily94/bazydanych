#ifndef PANELKLIENTA_H
#define PANELKLIENTA_H

#include <QMainWindow>

namespace Ui {
class PanelKlienta;
}

class AnulujZlecenie;
class AktualizujBaze;

class PanelKlienta : public QMainWindow
{
    Q_OBJECT

public:
    explicit PanelKlienta(QWidget *parent = 0);
    ~PanelKlienta();
    void aktualizuj_liste();
    AnulujZlecenie *Cancel;
    AktualizujBaze *Update;
signals:
    void finished();
    void cancel(QString);
    void dodano();
private slots:
    void on_addBtn_clicked();

    void on_logoutBtn_clicked();

    void on_cancelBtn_clicked();

    void got_client_nr(QString ID);

    void aktualizuj();

private:
    Ui::PanelKlienta *ui;
    QString id_klienta;
    QString id_zlecenia;
    QString dostepnosc;
    QString id_rabat;
};

#endif // PANELKLIENTA_H
