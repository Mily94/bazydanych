#include "panelklienta.h"
#include "ui_panelklienta.h"
#include "login.h"
#include "anulujzlecenie.h"
#include "aktualizujbaze.h"
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

PanelKlienta::PanelKlienta(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PanelKlienta)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN4");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("szereg");
    if(!db.open()){
        ui->client_fail->setText("Błąd połączenia z bazą");
    }
    connect(this,SIGNAL(dodano()),this,SLOT(aktualizuj()));
    Update = new AktualizujBaze;
}

PanelKlienta::~PanelKlienta()
{
    delete ui;
}

void PanelKlienta::aktualizuj_liste()
{
    QString Pytanie;
    QString Pytanie2;
    QString Pytanie3;
    QString linia;
    QString id_usl;
    QString plan_rozp;
    QString czas;
    QSqlDatabase db = QSqlDatabase::database("CONN4");
    QSqlQuery zapytanie(db);
    QSqlQuery zapytanie2(db);
    ui->oczekujace_lista->clear();
    ui->zrealizowane_lista->clear();
    Pytanie += "SELECT * FROM zlecenia WHERE id_klient = '";
    Pytanie += id_klienta;
    Pytanie2 = Pytanie;
    Pytanie += "' AND `status` = 'oczekiwanie'";
    Pytanie2 += "' AND `status` = 'zrealizowano'";


    zapytanie.exec(Pytanie);
    zapytanie2.exec(Pytanie3);
    if(zapytanie.size()>0){
        while(zapytanie.next()){
            linia.clear();
            linia += "Nr zlecenia: ";
            linia += zapytanie.value("id").toString();
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "Typ zlecenia: ";
            id_usl = zapytanie.value("id_usluga").toString();
            if(id_usl == "1"){
                linia += "Wiercenie 1";
            }
            if(id_usl == "2"){
                linia += "Wiercenie 2";
            }
            if(id_usl == "3"){
                linia += "Wiercenie 3";
            }
            if(id_usl == "4"){
                linia += "Wiercenie 4";
            }
            if(id_usl == "5"){
                linia += "Wiercenie 5";
            }
            if(id_usl == "6"){
                linia += "Wiercenie 6";
            }
            if(id_usl == "7"){
                linia += "Wiercenie 7";
            }
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "Dostęp. przedm: ";
            linia += zapytanie.value("dostepnosc").toString();
            ui->oczekujace_lista->append(linia);
            linia.clear();
            Pytanie3.clear();
            Pytanie3 += "SELECT * FROM usluga WHERE usluga.id = ";
            Pytanie3 += id_usl;
            plan_rozp = zapytanie.value("planowanytermin").toString();
            zapytanie2.exec(Pytanie3);
            if(zapytanie2.size()>0){
                while(zapytanie2.next()){
                    czas = zapytanie2.value("czastrwania").toString();
                }
            }
            linia += "Plan. rozpocz.: ";
            linia += QString::number(plan_rozp.toInt()-czas.toInt());
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "Plan. zakończ.: ";
            linia += plan_rozp;
            ui->oczekujace_lista->append(linia);
            ui->oczekujace_lista->append("\n");
        }
    }
    else{
        ui->oczekujace_lista->setText("Brak zleceń");
    }

    zapytanie.exec(Pytanie2);
    if(zapytanie.size()>0){
        while(zapytanie.next()){
            linia.clear();
            linia += "Numer zlecenia: ";
            linia += zapytanie.value("id").toString();
            ui->zrealizowane_lista->append(linia);
            linia.clear();
            linia += "Typ zlecenia: ";
            id_usl = zapytanie.value("id_usluga").toString();
            if(id_usl == "1"){
                linia += "Wiercenie 1";
            }
            if(id_usl == "2"){
                linia += "Wiercenie 2";
            }
            if(id_usl == "3"){
                linia += "Wiercenie 3";
            }
            if(id_usl == "4"){
                linia += "Wiercenie 4";
            }
            if(id_usl == "5"){
                linia += "Wiercenie 5";
            }
            if(id_usl == "6"){
                linia += "Wiercenie 6";
            }
            if(id_usl == "7"){
                linia += "Wiercenie 7";
            }
            ui->zrealizowane_lista->append(linia);
            linia.clear();
            linia += "Czas rozp.: ";
            linia += zapytanie.value("termin").toString();
            ui->zrealizowane_lista->append(linia);
            linia.clear();
            linia += "Czas zakończ.: ";
            linia += zapytanie.value("czas").toString();
            ui->zrealizowane_lista->append(linia);
            ui->zrealizowane_lista->append("\n");
        }
    }
    else{
        ui->zrealizowane_lista->setText("Brak zleceń");
    }

}

void PanelKlienta::on_addBtn_clicked()
{
QString Pytanie;
int id_zlecenia = ui->zlecenia_wybor->currentIndex()+1;
dostepnosc = ui->dostepnosc->text();
QSqlDatabase db = QSqlDatabase::database("CONN4");
QSqlQuery zapytanie(db);
Pytanie += "SELECT * FROM klient WHERE id = ";
Pytanie += id_klienta;
zapytanie.exec(Pytanie);
if(zapytanie.size()>0){
    while(zapytanie.next()){
        if(zapytanie.value("kartarabatowa").toString() == "tak"){
            id_rabat = "2";
        }
        else{
            id_rabat = "1";
        }
    }
}
Pytanie.clear();
Pytanie += "INSERT INTO zlecenia (id_klient, id_stawka, id_rabat, id_usluga, dostepnosc, status) VALUES ('";
Pytanie += id_klienta;
Pytanie += "', '";
Pytanie += "1";
Pytanie += "', '";
Pytanie += id_rabat;
Pytanie += "', '";
Pytanie += QString::number(id_zlecenia);
Pytanie += "', '";
Pytanie += dostepnosc;
Pytanie += "', 'oczekiwanie')";
zapytanie.exec(Pytanie);
emit dodano();
}

void PanelKlienta::on_logoutBtn_clicked()
{
  emit finished();
    this->close();
}

void PanelKlienta::on_cancelBtn_clicked()
{
    Cancel = new AnulujZlecenie;
    Cancel->show();
    connect(this,SIGNAL(cancel(QString)), Cancel, SLOT(got_client_nr(QString)));
    connect(Cancel, SIGNAL(deleted()), this, SLOT(aktualizuj()));
    emit cancel(id_klienta);
}

void PanelKlienta::got_client_nr(QString ID)
{
   id_klienta = ID;
   ui->IDnum->setText(id_klienta);
   aktualizuj_liste();
}

void PanelKlienta::aktualizuj()
{
    Update->znajdzRPQ();
    aktualizuj_liste();
}
