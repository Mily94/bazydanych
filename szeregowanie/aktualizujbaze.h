#ifndef AKTUALIZUJBAZE_H
#define AKTUALIZUJBAZE_H
#include <QMainWindow>

class Zlecenie;

class AktualizujBaze
{
public:
    AktualizujBaze();
    void znajdzRPQ();
    void Algorytm();
    int findMaxQindex(QVector<Zlecenie> vec);
    int findMinR(QVector<Zlecenie> vec);
    int findMinRindex(QVector<Zlecenie> vec);
    void wprowadzDoBazy();
    void aktualizujIndexy();
    int id;
private:
    QVector<Zlecenie> data;
    QVector<Zlecenie> N;
    QVector<Zlecenie> G;
    QVector<Zlecenie> permutation;
    QVector<int> perm;
};

#endif // AKTUALIZUJBAZE_H
