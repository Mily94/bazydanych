#include "clientdetails.h"
#include "ui_clientdetails.h"
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

ClientDetails::ClientDetails(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClientDetails)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN7");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("szereg");
    if(!db.open()){
        ui->details_debug->setText("Błąd połączenia z bazą");
    }
}

ClientDetails::~ClientDetails()
{
    delete ui;
}

void ClientDetails::wypisz_dane()
{
  QString Pytanie;
  QSqlDatabase db = QSqlDatabase::database("CONN7");
  QSqlQuery zapytanie(db);
  Pytanie += "SELECT * FROM klient WHERE id = ";
  Pytanie += id_klienta;

  zapytanie.exec(Pytanie);
  if(zapytanie.size()>0){
      while(zapytanie.next()){
            ui->id->setText(zapytanie.value("id").toString());
            ui->imie->setText(zapytanie.value("imie").toString());
            ui->nazwisko->setText(zapytanie.value("nazwisko").toString());
            ui->telefon->setText(zapytanie.value("telefon").toString());
            ui->karta->setText(zapytanie.value("kartarabatowa").toString());
  }
  }
  else{
      ui->details_debug->setText("Nie ma takiego klienta");
  }


}

void ClientDetails::on_pushButton_clicked()
{
    this->close();
}

void ClientDetails::got_client_nr(QString ID)
{
    id_klienta = ID;
    wypisz_dane();
}
