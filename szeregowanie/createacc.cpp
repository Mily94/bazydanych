#include "createacc.h"
#include "ui_createacc.h"
#include "login.h"
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

CreateAcc::CreateAcc(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CreateAcc)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN2");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("szereg");
    if(!db.open()){
        ui->create_fail->setText("Błąd połączenia z bazą");
    }

}

CreateAcc::~CreateAcc()
{
    delete ui;
}

void CreateAcc::on_createBtn_clicked()
{
    QString Pytanie;
    QSqlDatabase db = QSqlDatabase::database("CONN2");
    QSqlQuery zapytanie(db);
    imie = ui->name_line->text();
    nazwisko = ui->surname_line->text();
    telefon = ui->tel_number->text();
    login = ui->username_line->text();
    haslo = ui->password_line->text();
    if(ui->karta_rabatowa->isChecked()){
        karta = "tak";
    }
    else{
        karta = "nie";
    }
    if(imie.isEmpty() || nazwisko.isEmpty() || telefon.isEmpty() || login.isEmpty() || haslo.isEmpty()){
        ui->create_fail->setText("Co najmniej jedno z pól jest puste");
    }
    else{
        Pytanie += "INSERT INTO klient (imie, nazwisko, telefon, kartarabatowa, login, haslo) VALUES ('";
        Pytanie += imie;
        Pytanie += "', '";
        Pytanie += nazwisko;
        Pytanie += "', '";
        Pytanie += telefon;
        Pytanie += "', '";
        Pytanie += karta;
        Pytanie += "', '";
        Pytanie += login;
        Pytanie += "', '";
        Pytanie += haslo;
        Pytanie += "')";
        zapytanie.exec(Pytanie);
        Pytanie.clear();
        Pytanie += "SELECT * FROM klient WHERE login = '";
        Pytanie += login;
        Pytanie += "'";
        zapytanie.exec(Pytanie);
        if(zapytanie.size()>0){
            while(zapytanie.next()){
                ID = zapytanie.value("id").toString();
            }
        }
        emit registered(ID);
        this->close();
    }
}

void CreateAcc::on_returnBtn_clicked()
{
    emit finished();
    this->close();
}
