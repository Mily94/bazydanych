#ifndef ANULUJZLECENIE_H
#define ANULUJZLECENIE_H

#include <QMainWindow>

namespace Ui {
class AnulujZlecenie;
}

class AnulujZlecenie : public QMainWindow
{
    Q_OBJECT

public:
    explicit AnulujZlecenie(QWidget *parent = 0);
    ~AnulujZlecenie();

private slots:
    void on_returnBtn_clicked();
    void got_client_nr(QString ID);

    void on_cancelBtn_clicked();

signals:
    void finished();
    void deleted();

private:
    Ui::AnulujZlecenie *ui;
    QString id_klienta;
    QString id_zlecenia;
};

#endif // ANULUJZLECENIE_H
