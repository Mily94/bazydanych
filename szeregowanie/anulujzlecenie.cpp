#include "anulujzlecenie.h"
#include "ui_anulujzlecenie.h"
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

AnulujZlecenie::AnulujZlecenie(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AnulujZlecenie)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN5");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("szereg");
    if(!db.open()){
        ui->cancel_fail->setText("Błąd połączenia z bazą");
    }
}

AnulujZlecenie::~AnulujZlecenie()
{
    delete ui;
}

void AnulujZlecenie::on_returnBtn_clicked()
{
    emit finished();
    this->close();
}

void AnulujZlecenie::got_client_nr(QString ID)
{
    id_klienta = ID;
}

void AnulujZlecenie::on_cancelBtn_clicked()
{
    QString Pytanie;
    QSqlDatabase db = QSqlDatabase::database("CONN5");
    QSqlQuery zapytanie(db);
    id_zlecenia = ui->cancel_line->text();
    Pytanie += "DELETE FROM zlecenia WHERE id = ";
    Pytanie += id_zlecenia;
    Pytanie += " AND id_klient = ";
    Pytanie += id_klienta;
    Pytanie += " AND `status` = 'oczekiwanie'";
    zapytanie.exec(Pytanie);
    if(zapytanie.numRowsAffected()>0){
        ui->cancel_fail->setText("Pomyślnie usunięto zlecenie");
        emit deleted();
    }
    else{
        ui->cancel_fail->setText("Nie ma takiego zlecenia, lub zostało zrealizowane");
    }
}
