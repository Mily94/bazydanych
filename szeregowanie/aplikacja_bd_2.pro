#-------------------------------------------------
#
# Project created by QtCreator 2017-05-21T18:38:56
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = aplikacja_bd_2
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        login.cpp \
    createacc.cpp \
    panelklienta.cpp \
    anulujzlecenie.cpp \
    paneladmina.cpp \
    aktualizujbaze.cpp \
    zlecenie.cpp \
    clientdetails.cpp

HEADERS  += login.h \
    createacc.h \
    panelklienta.h \
    anulujzlecenie.h \
    paneladmina.h \
    aktualizujbaze.h \
    zlecenie.h \
    clientdetails.h

FORMS    += login.ui \
    createacc.ui \
    panelklienta.ui \
    anulujzlecenie.ui \
    paneladmina.ui \
    clientdetails.ui


win32: LIBS += -L$$PWD/../connector/lib/ -llibmysql

INCLUDEPATH += $$PWD/../connector/include
DEPENDPATH += $$PWD/../connector/include

win32: LIBS += -L$$PWD/../../download/mysql-connector-c-6.1.10-win32/lib/ -llibmysql

INCLUDEPATH += $$PWD/../../download/mysql-connector-c-6.1.10-win32/include
DEPENDPATH += $$PWD/../../download/mysql-connector-c-6.1.10-win32/include
