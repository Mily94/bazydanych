#ifndef CLIENTDETAILS_H
#define CLIENTDETAILS_H

#include <QMainWindow>

namespace Ui {
class ClientDetails;
}

class ClientDetails : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClientDetails(QWidget *parent = 0);
    ~ClientDetails();
    void wypisz_dane();

private slots:
    void on_pushButton_clicked();
    void got_client_nr(QString ID);

private:
    Ui::ClientDetails *ui;
    QString id_klienta;
};

#endif // CLIENTDETAILS_H
