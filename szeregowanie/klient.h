#ifndef KLIENT_H
#define KLIENT_H
#include <QMainWindow>

class Klient
{
public:
    Klient(QString login1, QString haslo1, QString imie1, QString nazwisko1, int telefon1, int karta1);
private:
    QString imie;
    QString nazwisko;
    int telefon;
    int karta;
    QString login;
    QString haslo;
    int id;
};

#endif // KLIENT_H
