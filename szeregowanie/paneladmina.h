#ifndef PANELADMINA_H
#define PANELADMINA_H

#include <QMainWindow>

namespace Ui {
class PanelAdmina;
}

class AktualizujBaze;
class ClientDetails;
class PanelAdmina : public QMainWindow
{
    Q_OBJECT

public:
    explicit PanelAdmina(QWidget *parent = 0);
    ~PanelAdmina();
    void aktualizuj_liste();
    void lista_klientow();
    AktualizujBaze *Update;
    ClientDetails *Detale;

signals:
    void finished();
    void details(QString);

private slots:
    void on_logoutBtn_clicked();

    void on_filterBtn_clicked();

    void aktualizuj();

    void on_updateBtn_clicked();

    void on_detailsBtn_clicked();

    void on_pushButton_clicked();

private:
    Ui::PanelAdmina *ui;
};

#endif // PANELADMINA_H
