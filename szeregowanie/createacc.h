#ifndef CREATEACC_H
#define CREATEACC_H

#include <QMainWindow>

namespace Ui {
class CreateAcc;
}

class CreateAcc : public QMainWindow
{
    Q_OBJECT

public:
    explicit CreateAcc(QWidget *parent = 0);
    ~CreateAcc();

private slots:
    void on_createBtn_clicked();

    void on_returnBtn_clicked();

signals:
    void finished();
    void registered(QString);

private:
    Ui::CreateAcc *ui;
    QString imie;
    QString nazwisko;
    QString login;
    QString haslo;
    QString telefon;
    QString karta;
    QString ID;
};

#endif // CREATEACC_H
