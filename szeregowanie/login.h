#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>

namespace Ui {
class Login;
}
class PanelAdmina;
class CreateAcc;
class PanelKlienta;
class QSqlDatabase;

class Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();
    PanelAdmina *Admin;
    PanelKlienta *Klient;
    CreateAcc *NKonto;

private slots:
    void on_loginBtn_clicked();

    void on_createAccBtn_clicked();

    void new_account(QString ID);

signals:
    void logged(QString ID);

private:
    Ui::Login *ui;
};

#endif // LOGIN_H
