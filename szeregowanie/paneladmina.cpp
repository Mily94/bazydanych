#include "paneladmina.h"
#include "ui_paneladmina.h"
#include "login.h"
#include "aktualizujbaze.h"
#include "clientdetails.h"
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

PanelAdmina::PanelAdmina(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PanelAdmina)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_DeleteOnClose);
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN3");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("szereg");
    if(!db.open()){
        ui->admin_fail->setText("Błąd połączenia z bazą");
    }
    else{
        aktualizuj_liste();
        lista_klientow();
    }
}

PanelAdmina::~PanelAdmina()
{
    delete ui;
}

void PanelAdmina::aktualizuj_liste()
{
    QString Pytanie;
    QString Pytanie2;
    QString Pytanie3;
    QString Pytanie4;
    QString linia;
    QString id_usl;
    QString plan_rozp;
    QString czas;
    QSqlDatabase db = QSqlDatabase::database("CONN3");
    QSqlQuery zapytanie(db);
    QSqlQuery zapytanie2(db);
    QString filtrid = ui->surname_filter->text();
    QString id_klienta;
ui->oczekujace_lista->clear();
ui->zrealizowane_lista->clear();
    Pytanie += "SELECT * FROM zlecenia WHERE ";
    if(!filtrid.isEmpty()){
        Pytanie2 += "SELECT * FROM klient WHERE id = '";
        Pytanie2 += filtrid;
        Pytanie2 += "'";
        zapytanie.exec(Pytanie2);
        if(zapytanie.size()>0){
            while(zapytanie.next()){
                id_klienta = zapytanie.value("id").toString();
                    Pytanie += "id_klient = ";
                    Pytanie += id_klienta;
                    if(zapytanie.next()){
                    Pytanie += " OR ";
                }
                    else{
                        Pytanie += " AND ";
                    }
            }
        }
    }
    Pytanie3 += Pytanie;
    Pytanie3 += "`status` = 'zrealizowano' ORDER BY termin ASC";
    Pytanie += "`status` = 'oczekiwanie'";
    zapytanie.exec(Pytanie);
    if(zapytanie.size()>0){
        while(zapytanie.next()){
            linia.clear();
            linia += "Numer zlecenia: ";
            linia += zapytanie.value("id").toString();
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "ID klienta: ";
            linia += zapytanie.value("id_klient").toString();
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "Typ zlecenia: ";
            id_usl = zapytanie.value("id_usluga").toString();
            if(id_usl == "1"){
                linia += "Wiercenie 1";
            }
            if(id_usl == "2"){
                linia += "Wiercenie 2";
            }
            if(id_usl == "3"){
                linia += "Wiercenie 3";
            }
            if(id_usl == "4"){
                linia += "Wiercenie 4";
            }
            if(id_usl == "5"){
                linia += "Wiercenie 5";
            }
            if(id_usl == "6"){
                linia += "Wiercenie 6";
            }
            if(id_usl == "7"){
                linia += "Wiercenie 7";
            }
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "Dostęp. przedm: ";
            linia += zapytanie.value("dostepnosc").toString();
            ui->oczekujace_lista->append(linia);
            linia.clear();
            Pytanie4.clear();
            Pytanie4 += "SELECT * FROM usluga WHERE usluga.id = ";
            Pytanie4 += id_usl;
            plan_rozp = zapytanie.value("planowanytermin").toString();
            zapytanie2.exec(Pytanie4);
            if(zapytanie2.size()>0){
                while(zapytanie2.next()){
                    czas = zapytanie2.value("czastrwania").toString();
                }
            }
            linia += "Plan. rozpocz.: ";
            linia += QString::number(plan_rozp.toInt()-czas.toInt());
            ui->oczekujace_lista->append(linia);
            linia.clear();
            linia += "Plan. zakończ.: ";
            linia += plan_rozp;
            ui->oczekujace_lista->append(linia);
            ui->oczekujace_lista->append("\n");
        }
    }
else{
    ui->oczekujace_lista->setText("Brak zleceń");
}


zapytanie.exec(Pytanie3);
if(zapytanie.size()>0){
    while(zapytanie.next()){
        linia.clear();
        linia += "Numer zlecenia: ";
        linia += zapytanie.value("id").toString();
        ui->zrealizowane_lista->append(linia);
        linia.clear();
        linia += "ID klienta: ";
        linia += zapytanie.value("id_klient").toString();
        ui->zrealizowane_lista->append(linia);
        linia.clear();
        linia += "Typ zlecenia: ";
        id_usl = zapytanie.value("id_usluga").toString();
        if(id_usl == "1"){
            linia += "Wiercenie 1";
        }
        if(id_usl == "2"){
            linia += "Wiercenie 2";
        }
        if(id_usl == "3"){
            linia += "Wiercenie 3";
        }
        if(id_usl == "4"){
            linia += "Wiercenie 4";
        }
        if(id_usl == "5"){
            linia += "Wiercenie 5";
        }
        if(id_usl == "6"){
            linia += "Wiercenie 6";
        }
        if(id_usl == "7"){
            linia += "Wiercenie 7";
        }
        ui->zrealizowane_lista->append(linia);
        linia.clear();
        linia += "Czas rozp.: ";
        linia += zapytanie.value("termin").toString();
        ui->zrealizowane_lista->append(linia);
        linia.clear();
        linia += "Czas zakończ.: ";
        linia += zapytanie.value("czas").toString();
        ui->zrealizowane_lista->append(linia);
        ui->zrealizowane_lista->append("\n");
    }
}
else{
    ui->zrealizowane_lista->setText("Brak zleceń");
}
}

void PanelAdmina::lista_klientow()
{
    QString Pytanie;
    QString linia;
    QSqlDatabase db = QSqlDatabase::database("CONN3");
    QSqlQuery zapytanie(db);

    Pytanie += "SELECT * FROM klient";
    ui->klienci_lista->clear();
    zapytanie.exec(Pytanie);
    if(zapytanie.size()>0){
        while(zapytanie.next()){
            linia.clear();
            linia += "ID = ";
            linia += zapytanie.value("id").toString();
            linia += " ";
            linia += zapytanie.value("imie").toString();
            linia += " ";
            linia += zapytanie.value("nazwisko").toString();
            ui->klienci_lista->append(linia);
        }
    }
    else{
        ui->klienci_lista->setText("Brak zarejestrowanych klientów");
    }
}

void PanelAdmina::on_logoutBtn_clicked()
{
    emit finished();
    this->close();
}

void PanelAdmina::on_filterBtn_clicked()
{
    emit aktualizuj();
}

void PanelAdmina::aktualizuj()
{
    aktualizuj_liste();
}

void PanelAdmina::on_updateBtn_clicked()
{
    QString Pytanie;
    QSqlDatabase db = QSqlDatabase::database("CONN3");
    QSqlQuery zapytanie(db);
    Pytanie += "SELECT * FROM zlecenia";
    zapytanie.exec(Pytanie);
    if(zapytanie.size()<=0){
        ui->admin_fail->setText("Nie ma żadnych zleceń");
    }
    else{
    Update = new AktualizujBaze;
    Update->aktualizujIndexy();
    Update->znajdzRPQ();
    Update->Algorytm();
    aktualizuj_liste();
    }
}

void PanelAdmina::on_detailsBtn_clicked()
{
    if(ui->klient_nr->text().isEmpty()){
        ui->admin_fail->setText("Nie wpisano numeru klienta");
    }
    else{
        Detale = new ClientDetails;
        connect(this, SIGNAL(details(QString)), Detale, SLOT(got_client_nr(QString)));
        ui->admin_fail->clear();
    Detale->show();
    emit details(ui->klient_nr->text());
    }
}

void PanelAdmina::on_pushButton_clicked()
{
    QString Pytanie;
    QSqlDatabase db = QSqlDatabase::database("CONN3");
    QSqlQuery zapytanie(db);

    Pytanie += "DELETE FROM zlecenia WHERE `status` = 'zrealizowano'";
    zapytanie.exec(Pytanie);
    aktualizuj_liste();

}
