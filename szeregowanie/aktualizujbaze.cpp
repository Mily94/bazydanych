#include "aktualizujbaze.h"
#include "zlecenie.h"
#include <QtSql/QtSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <iostream>

AktualizujBaze::AktualizujBaze()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "CONN6");
        db.setHostName("localhost");
        db.setUserName("root");
        db.setPassword("");
        db.setDatabaseName("szereg");
        db.open();
}

int AktualizujBaze::findMinRindex(QVector<Zlecenie> vec) {
    int min = vec[0].r;
    int minIndex = 0;
    for (int i = 0; i < (int) vec.size(); i++) {
        if (vec[i].r < min) {
            min = vec[i].r;
            minIndex = i;
        }
    }
    return minIndex;
}

void AktualizujBaze::wprowadzDoBazy()
{
    QString Pytanie;
    QSqlDatabase db = QSqlDatabase::database("CONN6");
    QSqlQuery zapytanie(db);
//UPDATE zlecenia SET termin=7, czas=8 WHERE id = 6
for(int i = 0; i < (int) data.size(); i++){
    Pytanie.clear();
    Pytanie += "UPDATE zlecenia SET termin = ";
    Pytanie += QString::number(data[i].start);
    Pytanie += ", czas = ";
    Pytanie += QString::number(data[i].finish);
    Pytanie += ", `status` = 'zrealizowano' ";
    Pytanie += " WHERE id = ";
    Pytanie += QString::number(data[i].id);
    zapytanie.exec(Pytanie);
}
}

void AktualizujBaze::aktualizujIndexy()
{
    QString Pytanie;
    QSqlDatabase db = QSqlDatabase::database("CONN6");
    QSqlQuery zapytanie(db);
    QSqlQuery zapytanie2(db);
    int i = 1;
    Pytanie = "SELECT * FROM zlecenia";
    zapytanie.exec(Pytanie);
    if(zapytanie.size()>0){
        while(zapytanie.next()){
            Pytanie.clear();
            Pytanie += "UPDATE zlecenia SET id = ";
            Pytanie += QString::number(i);
            Pytanie += " WHERE id = ";
            Pytanie += zapytanie.value("id").toString();
            zapytanie2.exec(Pytanie);
            ++i;
        }
    }
}

int AktualizujBaze::findMinR(QVector<Zlecenie> vec) {
    int min = vec[0].r;
    for (int i = 0; i < (int) vec.size(); i++) {
        if (vec[i].r < min)
            min = vec[i].r;
    }
    return min;
}

int AktualizujBaze::findMaxQindex(QVector<Zlecenie> vec) {
    int max = vec[0].q;
    int maxIndex = 0;
    for (int i = 0; i < (int) vec.size(); i++) {
        if (vec[i].q > max) {
            max = vec[i].q;
            maxIndex = i;
        }
    }
    return maxIndex;
}

void AktualizujBaze::znajdzRPQ()
{
    QSqlDatabase db = QSqlDatabase::database("CONN6");
    QSqlQuery mysqlquery(db);
    QSqlQuery mysqlquery2(db);
    QString Pytanie;
    Zlecenie task;
    mysqlquery.exec("SELECT * FROM zlecenia");
    mysqlquery2.exec("SELECT * FROM usluga, zlecenia WHERE usluga.id = zlecenia.id_usluga");

    if (mysqlquery.size() > 0) {
           while(mysqlquery.next()) {
               task.id = mysqlquery.value("id").toInt();
               task.r = mysqlquery.value("dostepnosc").toInt();
               task.status = mysqlquery.value("status").toString().toUtf8().constData();
               mysqlquery2.next();
               task.p = mysqlquery2.value("czastrwania").toInt();
               data.push_back(task);
           }
       }

    for (int i = 0; i < (int) data.size(); i++) {
            data[i].q = 3 * data[i].r + 2 * data[i].p;
            Pytanie.clear();
            Pytanie += "UPDATE zlecenia SET planowanytermin = ";
            Pytanie += QString::number(data[i].q);
            Pytanie += " WHERE id = ";
            Pytanie += QString::number(data[i].id);
            mysqlquery.exec(Pytanie);
    }
}

void AktualizujBaze::Algorytm()
{
    int t=0, Cmax=0;
    N = data;
    for (int i = 0; i < (int) N.size(); i++) {
           N[i].q = 0;
       }

       while (G.size() != 0 || N.size() != 0) {

              BACK:while (N.size() != 0 && findMinR(N) <= t) {
                  int minRindex = findMinRindex(N);
                  G.push_back(N[minRindex]);
                  N.erase(N.begin() + minRindex);
              }

              if (G.size() == 0) {
                  int minRindex = findMinRindex(N);
                  t = N[minRindex].r;
                  goto BACK;
              }

              int maxQindex = findMaxQindex(G);
              int p = G[maxQindex].p;
              int q = G[maxQindex].q;

              permutation.push_back(G[maxQindex]);
              perm.push_back(G[maxQindex].id);

              G.erase(G.begin() + maxQindex);

              t += p;

              if (Cmax < (t + q))
                  Cmax = t + q;
          }
         data[perm[0] - 1].start = data[perm[0] - 1].r;
         data[perm[0] - 1].finish = data[perm[0] - 1].r + data[perm[0] - 1].p;
         for (int i = 1; i < (int) data.size(); i++) {
             data[perm[i] - 1].start = data[perm[i-1] - 1].finish;
             data[perm[i] - 1].finish = data[perm[i] - 1].start + data[perm[i] - 1].p;
         }
         int delay;
           for (int i = 0; i < (int) data.size(); i++) {

               data[i].status = "zrealizowano";

               delay = data[i].finish - data[i].q;
               if (delay > 0) {
                   data[i].delay = delay;
               } else {
                   data[i].delay = 0;
               }
           }
        wprowadzDoBazy();
}


